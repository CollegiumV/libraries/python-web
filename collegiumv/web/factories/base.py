from flask import Flask
from collegiumv.web.settings import Defaults


class FlaskFactory(object):
    """Basic Factory for Flask Applications

    This class should be extended, overriding the CONTROLLERS value with
    any application blueprints.

    Attributes:
        CONTROLLERS (list of flask controllers): Controllers to register
        CONFIG (obj): Configuration to apply to the flask application

        FACTORY_CONTROLLERS (list of flask controllers): Additional Controllers
            Should only be modified by collegiumv.web factory extensions

        DEFAULT_CONFIG (obj): Default configuration to apply
    """
    CONTROLLERS = []
    CONFIG = None
    DEFAULT_CONFIG = Defaults
    CONFIG_ENV_VAR = 'CV_FLASK_SETTINGS'

    def __init__(self):
        """Initialize an app factory

        Creates a flask instance, configures it, and registers blueprints
        """
        self.app = Flask(__name__)
        self.configure()
        self.register_controllers()

    def configure(self):
        """Configure the flask application

        First configures the application with DEFAULT_CONFIG, then CONFIG,
        and finally from the environment.
        """
        self.app.config.from_object(self.DEFAULT_CONFIG)

        if self.CONFIG is not None:
            self.app.config.from_object(self.CONFIG)

    def register_controllers(self):
        """Registers all controllers for the application

        Configures blueprints in end application
        """
        for controller in self.CONTROLLERS:
            self.app.register_blueprint(controller)


# vim: ai ts=4 sts=4 et sw=4 ft=python
