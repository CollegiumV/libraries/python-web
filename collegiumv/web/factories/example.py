from collegiumv.web.factories.base import FlaskFactory
from collegiumv.web.controllers.ping import ping
from collegiumv.web.controllers.pong import pong


class PingFactory(FlaskFactory):
    """Ping Factory for applications that include ping controller"""

    def __init__(self):
        """Initialize parent and then self

        This call allows for (okay) multiple inheritance.
        After the super call, we register the ping blueprint
        """
        super(PingFactory, self).__init__()
        self.app.register_blueprint(ping)


class PongFactory(FlaskFactory):
    """Ping Factory for applications that include ping controller"""

    def __init__(self):
        """Initialize parent and then self

        This call allows for (okay) multiple inheritance.
        After the super call, we register the ping blueprint
        """
        super(PongFactory, self).__init__()
        self.app.register_blueprint(pong)


# vim: ai ts=4 sts=4 et sw=4 ft=python
