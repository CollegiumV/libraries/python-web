import os


class Defaults:
    SECRET_KEY = os.environ.get('CV_SECRET_KEY') or 'cvcvcvcv'
    DEBUG = os.environ.get('CV_DEBUG', 'false').lower() == "true"


# vim: ai ts=4 sts=4 et sw=4 ft=python
