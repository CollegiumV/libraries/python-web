"""Simple Ping/Pong Controller

This is primarily meant for the ExampleFactory implementation.

Adds a /pong endpoint that returns the word "ping"
"""
from flask import Blueprint

pong = Blueprint('pong', __name__)


@pong.route('/pong', methods=['GET'])
def pong_ping():
    return 'ping'


# vim: ai ts=4 sts=4 et sw=4 ft=python
