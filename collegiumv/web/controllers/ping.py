"""Simple Ping/Pong Controller

This is primarily meant for the ExampleFactory implementation.

Adds a /ping endpoint that returns the word "pong"
"""
from flask import Blueprint

ping = Blueprint('ping', __name__)


@ping.route('/ping', methods=['GET'])
def ping_pong():
    return 'pong'


# vim: ai ts=4 sts=4 et sw=4 ft=python
