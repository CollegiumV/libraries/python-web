# CollegiumV Python Web Helpers (for flask)

This repository contains general helper functionality for python flask applications,
with the goal of lowering the barrier to entry as much as possible for new development.

## Usage

This package is under the collegiumv namespace. Once installed, it can be imported:

```python
import collegiumv.web
```

## Using Factories

Factories could be used like so:

```python
app = PingFactory().app
```

However, often it is more useful to extend the functionality of a factory,
for example adding additional controllers:

```python
class AppFactory(PingFactory):
    CONTROLLERS = [ my_awesome_controller ]

app = AppFactory().app
```

If you need additional functionality, a safe way to override `__init__` can
be done like so:

```python
class AppFactory(PingFactory):
    def __init__(self):
        super(AppFactory, self).__init__()
        self.app.register_blueprint(my_awesome_controller)
        ...
```

## Using Multiple Factories

Consider the following example:

```python
from collegiumv.web.factories.example import PingFactory, PongFactory

class AppFactory(PingFactory, PongFactory):
    pass

app = AppFactory().app
```

The factories in this libary are safe to use with multiple inheritance,
in the above example, first the Base FlaskFactory configuration is run,
then the PongFactory, and finally the PingFactory.
