from setuptools import setup, find_packages

setup(
    name='collegiumv-web',
    version='0.1',
    description='Web resources for collegiumv',
    url='https://gitlab.com/collegiumv/libraries/python-web',
    author='cvadmins',
    license='ISC',
    packages=find_packages(),
    install_requires=['Flask'],
    zip_safe=False)
